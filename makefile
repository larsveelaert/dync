PROJECTNAME=$(shell basename $(CURDIR))
TAG=$(shell git rev-parse --short HEAD)

help:
	@echo "clean:      Cleans the repo of build remnants"
	@echo "deps:       Installs als needed dependencies to build, test & deploy the application"
	@echo "build:      Builds the application"

clean:
	@find . -name "*~" -type f -delete
	@echo 'Cleaned'

deps:
	@#Alpine
	@if type apk 1>/dev/null 2>/dev/null; then \
		apk update;\
		apk add musl-dev gcc;\
		apk add go git;\
	fi
	@#Mac
	@if type port 1>/dev/null 2>/dev/null; then \
		sudo port install golang git;\
	fi

test: clean
	go version
	go get ./...
	go test ./... -cover

build: clean
	go version
	go get ./...
	go build

intall: clean
	go version
	go get ./...
	go install
