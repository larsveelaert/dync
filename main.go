package main

import (
	"fmt"
	//"gitlab.com/larsveelaert/dync/internal/encryption"
	//"gitlab.com/larsveelaert/dync/internal/folders"
	"gitlab.com/larsveelaert/dync/internal/options"
	//"gitlab.com/larsveelaert/dync/internal/remotes"
	//"gitlab.com/larsveelaert/dync/internal/syncer"
	"io/ioutil"
	"log"
	"os"
	//"strconv"
)

func main() {
	//retrieve settings
	opt, err := options.Get()
	if err != nil {
		log.Fatal(err)
	}

	//configure runtime
	configure(opt)

	//execute command
	switch opt.Command {
	case "":
		err = sync(opt)
	case "sync":
		err = sync(opt)
	case "mount":
		fmt.Println("Command not yet implemented")
	case "offload":
		fmt.Println("Command not yet implemented")
	case "decrypt":
		err = decrypt(opt)
	case "encrypt":
		fmt.Println("Command not yet implemented")
	default:
		fmt.Println("'" + opt.Command + "' is not a command")
	}

	if err != nil {
		fmt.Print(err)
		os.Exit(1)
	}
}

func configure(opt *options.Options) error {
	err := toggleDebug(opt.Debug)
	if err != nil {
		return err
	}
	return nil
}

func toggleDebug(debugOption bool) error {
	if debugOption {
		fmt.Println("Debugging active")
	} else {
		log.SetOutput(ioutil.Discard)
	}
	return nil
}

func sync(opt *options.Options) error {
	//TODO prepLocations -> loop over all of them and assure the utterlease availability -> mount the necessairy things / or translate kbfs://

	//loop over all the folders that need to be synced
	/*
		for i, folder := range conf.Folders {
			//indicator of progress
			fmt.Println("[" + strconv.Itoa(i+1) + "/" + strconv.Itoa(len(conf.Folders)) + "]")

			//build slice of all locations that need to be synced
			locations := []syncer.Location{syncer.Location(folder)}
			for _, remote := range remotes.Get(folder.FullPath, conf.RemotesFile) {
				//check uniquesness of location
				var locationPresent bool
				for _, l := range locations {
					if remote == l.FullPath {
						locationPresent = true
						break
					}
				}

				//add to locations if not already present
				if !locationPresent {
					locations = append(locations, syncer.Location{remote})
				}
			}

			//sync all the locations
			s := syncer.Syncer{DryRun: conf.DryRun}
			err := s.Sync(locations)
			if err != nil {
				return err
			}
		}
	*/
	return nil
}

func decrypt(options *options.Options) error {
	/*
		for i, folder := range conf.Folders {
			//progress indicator
			fmt.Println("[" + strconv.Itoa(i+1) + "/" + strconv.Itoa(len(conf.Folders)) + "]")

			//start services if needed
			localPath, err := folders.AssureLocalAvailability(folder.FullPath)
			if err != nil {
				return err
			}

			fmt.Printf("[INFO] %s resolved to local path of %s\n", folder.FullPath, localPath)

			//detect encryption
			e, err := encryption.Detect(localPath, true)
			if err != nil {
				return err
			}

			//decrypt
			dyncDir, err := folders.GetDyncDir(localPath)
			if err != nil {
				return err
			}
			encryption.Decrypt(localPath, dyncDir, e)
		}
	*/
	return nil
}
