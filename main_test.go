package main

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestGetAliasesFromConf(t *testing.T) {
	// Create tmp
	tmpFile, err := ioutil.TempFile(os.TempDir(), "test-")
	if err != nil {
		log.Fatal("Cannot create temporary file", err)
	}
	// Remember to clean up the file afterwards
	defer os.Remove(tmpFile.Name())

	// Example writing to the file
	text := []byte(`[test]
location = "/test/test"
remote-caching = true`)

	if _, err = tmpFile.Write(text); err != nil {
		log.Fatal("Failed to write to temporary file", err)
	}

	// Close the file
	if err := tmpFile.Close(); err != nil {
		log.Fatal(err)
	}

	// Test reading of conf file
	config, err := getConf(tmpFile.Name())
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, config["test"].(map[string]interface{})["location"].(string), "/test/test")
	assert.Equal(t, config["test"].(map[string]interface{})["remote-caching"].(bool), true)
}
