# dync

Decentralized Sync with Git version control

-> osync gebruiken underneat -> make it easily changeble for unison or for another thingy
-> do think about folder structure with go and scripts ... go run 

-> is there a benifit to git??? (unison not well supported?), not inherent
-> instead of unison, use git, with a very strict clean policy?
-> will git double storage need?

-> if git has a pro -> do use it? or use it for smaller folders, to do more over time better changes?

Decentralized file sync with extra backends

-> which language will it be? python or bash????

# /keybase/private/<name>/<folder>

<name> from that one dotfile 
- keybase:lvlrt 

# dotfile ideas...
-> have a dotfile in every dir in homedir to sync 
e.g.
/keybase/private/lvlrt/<folder>
ssh://c1/<folder>
ssh://k1/<folder> 

-> if starts with /keybase/ -> do some extra safety check is keybase is operating and if not private -> to give a big disclaimer
0-> if its not running, start keybase
-> or fail??? give clear error


1-> voor de rest gewoon unison sync

->very clear error if it did not sync to anything

## PROPRIETERY SYNCER (RSYNC BACKED)
### Goals
- Backup and conflicting changes are backed up. All changes for every account
- Multi location, as fast as possible

### MVP
- Sync based on timestamp which is the latest onem and propagate deletions only if there is a deletion on that file, later than the latest change

### Steps
- Generate a list for every location of the complete directory with [file path] {hash: seufbsefbsfisbf, timestamp: 1235353532, state: DELETED | CURRENT}
- Timestamp on delete is previous sync from when it was lost (Must be carried through in history in every state file when it was last deleted, can be different file with all deletions)
- Timestamp on current is the last changed if the hash is different otherwise it is the last synced one

- Get a list of all paths of all locations (excemt .dync-workdir)
- Go over them and loop over all locations, find the state with the last timestamp
- this results in the final state

- every location has to be check against the final state and actions have to be defined
- overrides (changed files), deletes, new files
- Execute all these actions parent first (carefull with directories too not overwrite) -> just dont touch directories on changes
- On creates and deletes, only te parent file is enough
- ? How backup and resets -> store these 'changesets' along with the previous version in a folder dedicated for the sync on that microsecond -> every participant in that sync should receive every file, the moment before it is deleted
- Revert <timestamp> reverts all on all locations

-FOLDER
 -.dync
 -.dync-workdir
  -backup
   -123125251251
    -c1:/DATA
    -s1:/DATA
