module gitlab.com/larsveelaert/dync

require (
	github.com/pelletier/go-toml v1.6.0
	github.com/shurcooL/httpfs v0.0.0-20181222201310-74dc9339e414 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd
	github.com/stretchr/testify v1.3.0
)

go 1.13
