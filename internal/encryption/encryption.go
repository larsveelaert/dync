package encryption

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
)

//Detect detects the encryption on the location, and could ask to specify if in doubt
func Detect(location string, ask bool) (string, error) {
	if ok, _ := isGoCryptFs(location); ok {
		return "gocryptfs", nil
	}
	return "", errors.New("No encryption detected")
}

//Decrypt decrypts the location to the destionation with given encryption method
func Decrypt(location string, destination string, e string) {
	switch e {
	case "gocryptfs":
		fmt.Println("Decrypting " + location + " using GoCryptFS to " + destination + " ...")
		if !checkProgramInstalled("gocryptfs") {
			fmt.Println("Please install gocryptfs first")
			return
		}
		//TODO max time confiruable?
		runCommand([]string{"gocryptfs", "-i", "1h", location, destination})
	default:
		fmt.Println("decryption not implemented")
	}

}

func isGoCryptFs(location string) (bool, error) {
	if ok, err := fileExists(path.Join(location, "gocryptfs.conf")); !ok {
		return false, err
	}
	if ok, err := fileExists(path.Join(location, "gocryptfs.diriv")); !ok {
		return false, err
	}
	return true, nil
}

func fileExists(location string) (bool, error) {
	info, err := os.Stat(location)
	if err != nil {
		return false, err
	}
	if info.IsDir() {
		return false, nil
	}
	return true, nil
}

func checkProgramInstalled(program string) bool {
	cmd := exec.Command("/bin/sh", "-c", "command -v "+program)
	if err := cmd.Run(); err != nil {
		return false
	}
	return true
}

func runCommand(command []string) {
	cmd := exec.Command(command[0], command[1:]...)
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	cmd.Run()
	/*
		cmd := exec.Command(command[0], command[1:]...)
		stderr, _ := cmd.StderrPipe()
		stdout, _ := cmd.StdoutPipe()
		cmd.Start()
		go func() {
			outputReader(stderr)
		}()
		go func() {
			outputReader(stdout)
		}()
		cmd.Wait()
	*/
}

func outputReader(reader io.Reader) {
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		l := scanner.Text()
		fmt.Println(l)
	}
}
