package encryption

import (
	"runtime"
	"testing"
)

func TestCheckProgramInstalled(t *testing.T) {
	if CheckProgramInstalled("ls") == false {
		t.Error("ls command does not exist!")
	}
	if CheckProgramInstalled("ls111") == true {
		t.Error("ls111 command should not exist!")
	}
}

func TestRunCommand(t *testing.T) {
	if runtime.GOOS == "windows" {
		RunCommand([]string{"dir"})
	} else {
		RunCommand([]string{"echo", "-n"})
	}
}
