package folders

import (
	"errors"
	"gitlab.com/larsveelaert/dync/internal/services"
	"os"
	"path"
	"strings"
)

//AssureLocalAvailability translates a full URI to where it is made available locally (will mount if needed)
func AssureLocalAvailability(URI string) (string, error) {
	if strings.HasPrefix(URI, string(os.PathSeparator)) { //local
		var localPath string
		if strings.HasPrefix(URI, "/keybase/") {
			keybasePath, err := services.ExtractKeybasePath(URI)
			if err != nil {
				return "", err
			}
			localPath, err = assureLocalAvailabilityKeybase(keybasePath)
			if err != nil {
				return "", err
			}
			return localPath, nil
		} else {
			return URI, nil
		}
	} else if strings.Contains(URI, "://") {
		protocol := strings.SplitN(URI, "://", 2)[0]
		var localPath string
		switch protocol {
		case "kbfs", "keybase", "kb":
			keybasePath, err := services.ExtractKeybasePath(URI)
			if err != nil {
				return "", err
			}
			localPath, err = assureLocalAvailabilityKeybase(keybasePath)
			if err != nil {
				return "", err
			}
			return localPath, nil
		default:
			return "", errors.New("Protocol used by " + URI + " is not implemented")
		}

	}

	return "", errors.New("URI " + URI + " can not be translated to a known location")
}

func assureLocalAvailabilityKeybase(keybasePath string) (string, error) {
	//check if running
	running, err := services.AssureKeybaseRunning()
	if err != nil {
		return "", err
	}
	if !running {
		return "", errors.New("Keybase does not appear to be running")
	}

	//find out the location
	keybaseFolder, err := services.GetKeybaseFolder()
	if err != nil {
		return "", err
	}

	//parse local path
	localPath := path.Join(keybaseFolder, keybasePath)

	//check if folder exists
	//TODO

	//promote the offline (caching) feature and how to enable (mention)
	err = services.ReportKeybaseFolderCached(keybasePath)
	if err != nil {
		return "", err
	}

	return localPath, nil

}
