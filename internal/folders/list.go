package folders

import (
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"
)

//ListWithConf lists all folders in the home dir that all folders from home folder that have a .dync file
func ListWithConf() ([]string, error) {
	//TODO allow a location to be given to override the homedir
	homeDir, err := GetHomeDir()
	if err != nil {
		return nil, err
	}
	location := homeDir

	var foldersWithConf []string
	for _, l := range filterDirs(readDir(location)) {
		if hasConf(path.Join(location, l.Name())) {
			foldersWithConf = append(foldersWithConf, path.Join(location, l.Name()))
		}
	}
	log.Println("Folders with configuration file: " + strings.Join(foldersWithConf, ","))
	return foldersWithConf, nil
}

func hasConf(dir string) bool {
	for _, m := range readDir(dir) {
		if m.Name() == ".dync" {
			return true
		}
	}
	return false
}

func filterDirs(list []os.FileInfo) []os.FileInfo {
	var dirList []os.FileInfo
	for _, l := range list {
		if l.IsDir() {
			dirList = append(dirList, l)
		}
	}
	return dirList
}

func readDir(location string) []os.FileInfo {
	files, err := ioutil.ReadDir(location)
	if err != nil {
		log.Fatal(err)
	}
	return files
}
