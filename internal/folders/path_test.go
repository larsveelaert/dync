package folders

import (
	"strings"
	"testing"
)

func TestGetHomeDir(t *testing.T) {
	homedir := GetHomeDir()
	if !strings.Contains(homedir, "/") {
		t.Fatalf("Homedir seems incorrect:" + homedir)
	}
}
