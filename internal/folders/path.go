package folders

import (
	"log"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"strings"
)

func GetFullPath(name string) (string, error) {
	if strings.Contains(name, "://") {
		return name, nil
	} else if !strings.HasPrefix(name, string(os.PathSeparator)) {
		homeDir, err := GetHomeDir()
		if err != nil {
			return "", err
		}

		return path.Join(homeDir, name), nil
	}
	return name, nil
}

func GetHomeDir() (string, error) {
	user, err := user.Current()
	if err != nil {
		return "", err
	}
	log.Println("User Homedir:" + user.HomeDir)
	return user.HomeDir, nil
}

//GetDyncDir returns the full path of the resulting directory to be created by dync
func GetDyncDir(name string) (string, error) {
	homedir, err := GetHomeDir()
	if err != nil {
		return "", err
	}
	return path.Join(homedir, filepath.Base(name)+".dync"), nil
}
