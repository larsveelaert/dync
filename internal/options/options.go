package options

import (
	"encoding/json"
	"flag"
	"github.com/pelletier/go-toml"
	"gitlab.com/larsveelaert/dync/internal/utils"
)

type Config struct {
	Args                 []string `json:",omitempty"`
	Command              *string  `json:",omitempty"`
	Debug                *bool    `json:",omitempty"`
	DryRun               *bool    `json:",omitempty"`
	GlobalConfigFilepath *string  `json:",omitempty"`
	LocalConfigFilepath  *string  `json:",omitempty"`
}

type Options struct {
	Args                 []string
	Command              string
	DryRun               bool
	Debug                bool
	GlobalConfigFilepath string
	LocalConfigFilepath  string
}

var defaults = Options{
	Command:              "sync",
	Debug:                false,
	DryRun:               false,
	GlobalConfigFilepath: "~/.dync",
	LocalConfigFilepath:  ".dync",
}

//All gives back all settings
func Get() (*Options, error) {
	//start with defaults
	options := defaults

	//parse command line options
	flags, err := getFlags()
	if err != nil {
		return nil, err
	}

	//check set global config filepath
	globalConfigFilepath := defaults.GlobalConfigFilepath
	if flags.GlobalConfigFilepath != nil {
		globalConfigFilepath = *flags.GlobalConfigFilepath
	}

	//retrieve config file, pipe em over into options, (if not nil?)
	globalConfig, err := getGlobalConfig(globalConfigFilepath)
	if err != nil {
		return nil, err
	}

	//overwrite default options if specified somewhere else
	overrideOptionsWithConfig(&options, globalConfig)
	overrideOptionsWithConfig(&options, flags)

	return &options, nil
}

func overrideOptionsWithConfig(options *Options, config *Config) {
	var m map[string]interface{}

	//marshal to json to merge together
	jOptions, _ := json.Marshal(options)
	json.Unmarshal(jOptions, &m)

	jConfig, _ := json.Marshal(config)
	json.Unmarshal(jConfig, &m)

	//marshal to struct
	jm, _ := json.Marshal(m)
	json.Unmarshal(jm, options)
}

func getFlags() (*Config, error) {
	//parse all values of the flags (if not given, gives default)
	flagValues := new(Config)
	flagValues.Debug = flag.Bool("Debug", defaults.Debug, "Show debug messages")
	flagValues.DryRun = flag.Bool("Dry", defaults.DryRun, "Run the command without preforming any changes")
	flagValues.LocalConfigFilepath = flag.String("LocalConfigFilepath", defaults.LocalConfigFilepath, "Defines path of config file with folder-specific settings")
	flagValues.GlobalConfigFilepath = flag.String("GlobalConfigFilepath", defaults.GlobalConfigFilepath, "Defines path of global config file with settings")
	flag.Parse()

	//leave all non-given flags to nil
	flags := new(Config)
	flag.Visit(func(f *flag.Flag) {
		switch f.Name {
		case "Debug":
			flags.Debug = flagValues.Debug
		case "DryRun":
			flags.DryRun = flagValues.DryRun
		case "LocalConfigFilepath":
			flags.LocalConfigFilepath = flagValues.LocalConfigFilepath
		case "GlobalConfigFilepath":
			flags.GlobalConfigFilepath = flagValues.GlobalConfigFilepath
		}
	})

	//get remaining arguments
	flags.Args = flag.Args()
	return flags, nil
}

func getGlobalConfig(filepath string) (*Config, error) {
	//expand filepath
	filepath, err := utils.ExpandFilePath(filepath)
	if err != nil {
		return nil, err
	}

	//read file
	contents, err := toml.LoadFile(filepath)
	if err != nil {
		return nil, err
	}

	//recursivly make map
	contentsMap := contents.ToMap()
	//marshal to struct
	config := new(Config)
	jm, err := json.Marshal(contentsMap)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(jm, config)
	if err != nil {
		return nil, err
	}

	return config, nil
}
