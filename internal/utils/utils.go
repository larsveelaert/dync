package utils

import (
	"bufio"
	"fmt"
	"io"
	"os/exec"
	"os/user"
	"strings"
)

func ExpandFilePath(filepath string) (string, error) {
	if strings.Contains(filepath, "~") {
		usr, _ := user.Current()
		homedir := usr.HomeDir
		filepath = strings.Replace(filepath, "~", homedir, 1)
	}
	return filepath, nil
}

//CheckProgramInstalled checks if a program (given in string) is installed and reachable on the system
func CheckProgramInstalled(program string) bool {
	cmd := exec.Command("/bin/sh", "-c", "command -v "+program)
	if err := cmd.Run(); err != nil {
		return false
	}
	return true
}

//RunCommand runs a command while outputting realtime
func RunCommand(command []string) {
	cmd := exec.Command(command[0], command[1:]...)
	stderr, _ := cmd.StderrPipe()
	stdout, _ := cmd.StdoutPipe()
	cmd.Start()
	go func() {
		outputReader(stderr)
	}()
	go func() {
		outputReader(stdout)
	}()
	cmd.Wait()
	return
}

func outputReader(reader io.Reader) {
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		l := scanner.Text()
		fmt.Println(l)
	}
}
