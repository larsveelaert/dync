package syncer

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

//State is struct to hold info about a file, current or history
type State struct {
	Hash      string
	Timestamp int64
	State     string
}

//GetLocationStates generates the location states for all locations
func getLocationStates(uid string, locations []Location, histories map[string]interface{}, dry bool) map[string]interface{} {
	locationStates := make(map[string]interface{})
	for _, location := range locations {
		log.Println("Retrieving location state from " + location.FullPath)
		if locationExists(location.FullPath, dry) {
			locationStates[location.FullPath] = generateState(uid, location.FullPath, histories[location.FullPath].(map[string]State))
		}
	}
	return locationStates
}

//Generate makes a map for the state of the location
func generateState(uid string, location string, history map[string]State) map[string]State {
	//TODO //TODO here if the file did not change ... use hash from history

	locationState := make(map[string]State)
	if isLocal(location) {
		localizedLocation := localizeLocation(location)
		log.Println("Retrieving all paths")
		allPaths := getAllPaths(location)
		lenAllPaths := strconv.Itoa(len(allPaths))
		log.Println("Processing paths into state (" + lenAllPaths + " total)")
		for i, path := range allPaths {
			log.Println(strconv.Itoa(i+1) + "/" + lenAllPaths)
			relativePath := strings.Replace(path, localizedLocation, "", 1)
			timestamp := getFileTimestamp(path)
			//TODO how search in history?
			//TODO TODO TODO if filestamp is different than history, then hash it
			hash := hashFile(path)
			locationState[relativePath] = State{Hash: hash, Timestamp: timestamp, State: "PRESENT"}
		}
	} else {
		//TODO this function has to be called in another exposed command -> that writes it
		//TODO run the remote command dync
		//dync --generate-state --id=123456789 => generates the state and saves it under /states , retrieved withh rsync
		//TODO and read it back in
		//TODO if remote does not have dync -> use rsync
		//rsync --list-only username@servername:/directoryname/
		//TODO only use timestamp and no hash
		//TODO if not commands can be done -> implement other ways of timestamping
	}

	//TODO TODO TODO do the depth map thing
	//depthMap := utils.GetDepthMap(extractPathsLocationState(locationState))
	//TODO recursive correct the timestamps of directories
	//TODO no idea what this is anymore? -> performance? -> copy dirs in full?

	return removeExcludedPaths(locationState)
}

func extractPathsLocationState(locationState map[string]State) []string {
	//TODO replace with a reflect??
	var paths []string
	for path := range locationState {
		paths = append(paths, path)
	}
	return paths
}

func removeExcludedPaths(locationState map[string]State) map[string]State {
	tempLocationState := locationState
	for path := range locationState {
		if strings.HasPrefix(path, workdir) || path == "" {
			delete(tempLocationState, path)
		}
	}
	return tempLocationState
}

//Sync synchronizes all the states into to 1 synced state
func syncStates(uid string, locationStates map[string]interface{}, histories map[string]interface{}) map[string]State {
	fmt.Println("Calculating synced state...")
	syncedState := make(map[string]State)
	log.Printf("SYNC: Calculate synced state of present files")
	for location, locationState := range locationStates {
		log.Printf("SYNC: Checking files of %s to calculate synced state", location)
		for path, state := range locationState.(map[string]State) {
			log.Printf("SYNC: Checking %s on %s", path, location)
			//Only used the current state, if it really differs from what was there (based on hash)
			//Otherwise a copy of the entirefile can render the history useless, because all files have new
			pathHash := encryptPath(uid, path)
			log.Printf("SYNC: %s is %s", path, pathHash)
			var tempState State

			if historyState, ok := histories[location].(map[string]State)[pathHash]; ok && historyState.Hash == state.Hash && historyState.State == "PRESENT" {
				log.Printf("SYNC: %s on %s is present, has history and did not change, keeping state from history", path, location)
				tempState = historyState
			} else {
				tempState = state
				if historyState.State != "PRESENT" {
					log.Printf("SYNC: %s on %s was not present and is present now, using the current state", path, location)
				}
				if historyState.Hash != state.Hash {
					log.Printf("SYNC: %s on %s is present and content changed, using the current state", path, location)
				}
			}

			log.Printf("SYNC: Comparing %s from %s to current projected synced state", path, location)
			if _, ok := syncedState[path]; ok {
				log.Printf("SYNC: %s already present in current projected synced state", path)
				if tempState.Hash == syncedState[path].Hash {
					log.Printf("SYNC: %s from %s is the same as in current projected synced state, using youngest timestamp", path, location)
					if tempState.Timestamp < syncedState[path].Timestamp {
						log.Printf("SYNC: %s from %s is younger", path, location)
						syncedState[path] = tempState
					} else {
						log.Printf("SYNC: %s from %s is older", path, location)
					}
				} else {
					log.Printf("SYNC: %s from %s is the different as in current projected synced state, using oldest timestamp", path, location)
					if tempState.Timestamp < syncedState[path].Timestamp {
						log.Printf("SYNC: %s from %s is younger", path, location)
					} else {
						log.Printf("SYNC: %s from %s is older", path, location)
						syncedState[path] = tempState
					}
				}
			} else {
				log.Printf("SYNC: %s not yet present in current projected synced state, using it's calculated state from %s", path, location)
				syncedState[path] = tempState
			}
		}
	}

	log.Printf("SYNC: Calculate deleted files to remove from projected synced state")
	for path, state := range syncedState {
		log.Printf("SYNC: Looking at %s", path)
		pathHash := encryptPath(uid, path)
		log.Printf("SYNC: %s is %s", path, pathHash)
		for location, locationState := range locationStates {
			if _, ok := locationState.(map[string]State)[path]; !ok { //if not exists in the location
				log.Printf("SYNC: %s does not exist on %s", path, location)
				if historyState, ok := histories[location].(map[string]State)[pathHash]; ok && historyState.State == "PRESENT" { //if in history
					log.Printf("SYNC: %s on %s was present in history of location and does not exists anymore", path, location)
					if historyState.Timestamp >= state.Timestamp {
						log.Printf("SYNC: %s on %s was present, does not exist anymore and had the most recent timestamp in history compared with to the projected synced state, it will be deleted", path, location)
						syncedState[path] = State{Hash: historyState.Hash, Timestamp: time.Now().Unix(), State: "DELETED"}
					}
				}
			}
		}
	}
	return removeExcludedPaths(syncedState)
}

func existsInFileStateMap(FileStateMap map[string]State, key string) bool {
	if _, ok := FileStateMap[key]; ok {
		return true
	}
	return false
}
