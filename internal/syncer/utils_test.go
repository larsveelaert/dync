package syncer

import (
	"encoding/hex"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"runtime"
	"sort"
	"strings"
	"testing"
	"time"
)

func TestCheckProgramInstalled(t *testing.T) {
	if CheckProgramInstalled("ls") == false {
		t.Error("ls command does not exist!")
	}
	if CheckProgramInstalled("ls111") == true {
		t.Error("ls111 command should not exist!")
	}
}

func TestRunCommand(t *testing.T) {
	if runtime.GOOS == "windows" {
		RunCommand([]string{"dir"})
	} else {
		RunCommand([]string{"echo", "-n"})
	}
}

func TestGetAllPaths(t *testing.T) {
	//TODO create small tree

	dir, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir) // clean up

	os.MkdirAll(filepath.Join(dir, "dir1"), os.ModePerm)
	os.MkdirAll(filepath.Join(dir, "dir2"), os.ModePerm)
	os.MkdirAll(filepath.Join(dir, "dir1", "dir11"), os.ModePerm)

	_, _ = os.Create(filepath.Join(dir, "file0"))
	_, _ = os.Create(filepath.Join(dir, "dir1", "file1"))
	_, _ = os.Create(filepath.Join(dir, "dir2", "file2"))
	_, _ = os.Create(filepath.Join(dir, "dir1", "dir11", "file11"))

	allPathsWant := []string{
		dir,
		filepath.Join(dir, "dir1"),
		filepath.Join(dir, "dir1", "dir11"),
		filepath.Join(dir, "dir1", "dir11", "file11"),
		filepath.Join(dir, "dir1", "file1"),
		filepath.Join(dir, "dir2"),
		filepath.Join(dir, "dir2", "file2"),
		filepath.Join(dir, "file0"),
	}
	allPathsHave := GetAllPaths(dir)

	sort.Strings(allPathsWant)
	sort.Strings(allPathsHave)

	assert.Equal(t, allPathsWant, allPathsHave)
}

func TestGetDepthMap(t *testing.T) {
	expected := make(map[int][]string)
	expected[0] = []string{"dir1"}
	expected[1] = []string{"dir1/dir11"}

	result := GetDepthMap([]string{"dir1/dir11", "dir1"})
	assert.Equal(t, expected, result, "")
}

func TestCalculateDepth(t *testing.T) {
	assert.Equal(t, calculateDepth("dir1"), 0)
	assert.Equal(t, calculateDepth("dir2/dir22"), 1)
}

func TestEncryptPath(t *testing.T) {
	encryptedString := EncryptPath("UID", "/test/file")
	_, err := hex.DecodeString(encryptedString)
	if err != nil {
		t.Fail() //must be hex
	}
}

func TestHashFile(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir) // clean up

	os.MkdirAll(filepath.Join(dir, "dir"), os.ModePerm)
	_, _ = os.Create(filepath.Join(dir, "file"))
	os.Symlink(filepath.Join(dir, "file"), filepath.Join(dir, "symlink"))

	assert.Equal(t, "directory", HashFile(filepath.Join(dir, "dir")))
	assert.Equal(t, "d41d8cd98f00b204e9800998ecf8427e", HashFile(filepath.Join(dir, "file")))
	_, err = hex.DecodeString(HashFile(filepath.Join(dir, "symlink")))
	if err != nil {
		t.Fail() //must be hex
	}
}

func TestGetFileTimestamp(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir) // clean up

	_, _ = os.Create(filepath.Join(dir, "file"))
	_ = os.Symlink(filepath.Join(dir, "file"), filepath.Join(dir, "symlink"))

	currenttime := time.Now().Local()

	err = os.Chtimes(filepath.Join(dir, "file"), currenttime, currenttime)
	if err != nil {
		log.Println(err)
	}
	err = os.Chtimes(filepath.Join(dir, "symlink"), currenttime, currenttime)
	if err != nil {
		log.Println(err)
	}

	assert.Equal(t, currenttime.Unix(), GetFileTimestamp(filepath.Join(dir, "file")))
	assert.Equal(t, currenttime.Unix(), GetFileTimestamp(filepath.Join(dir, "symlink")))
}

func TestCleanLocations(t *testing.T) {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	home := usr.HomeDir

	var tests = []struct {
		locations        []string
		cleanedLocations []string
	}{
		{
			[]string{},
			[]string{},
		},
		{
			[]string{"~/test"},
			[]string{home + "/test/"},
		},
		{
			[]string{"~/test/"},
			[]string{home + "/test/"},
		},
		{
			[]string{"/test"},
			[]string{"/test/"},
		},
		{
			[]string{"/test/"},
			[]string{"/test/"},
		},
	}

	for _, test := range tests {
		t.Run(strings.Join(test.locations, ","), func(t *testing.T) {
			have := CleanLocations(test.locations)
			assert.Equal(t, test.cleanedLocations, have)
		})
	}

}

func TestIsLocal(t *testing.T) {
	var tests = []struct {
		location string
		expected bool
	}{
		{
			"/test",
			true,
		},
		{
			"ssh://test",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.location, func(t *testing.T) {
			assert.Equal(t, test.expected, IsLocal(test.location))
		})
	}

}

func TestLocationExists(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir) // clean up

	//Local existing location
	//Local Non-existing location

	//TODO do function and check if location now exists
	//+ True

	//TODO dry non dry
	var tests = []struct {
		dir          string
		create       bool
		dry          bool
		wantResponse bool
		wantExist    bool
	}{
		{
			"dir1",
			true,
			true,
			true,
			true,
		},
		{
			"dir2",
			false,
			true,
			true,
			false,
		},
		{
			"dir3",
			false,
			false,
			true,
			true,
		},
	}

	for _, test := range tests {
		t.Run(test.dir, func(t *testing.T) {
			if test.create {
				os.MkdirAll(filepath.Join(dir, test.dir), os.ModePerm)
			}
			//Response
			assert.Equal(t, test.wantResponse, LocationExists(filepath.Join(dir, test.dir), test.dry))
			//Exists?
			_, err := os.Stat(filepath.Join(dir, test.dir))
			assert.Equal(t, test.wantExist, !os.IsNotExist(err))
		})
	}

}

//Localizelocation
//SuffixSlash
//IsPath
//ListOnlyFiles
