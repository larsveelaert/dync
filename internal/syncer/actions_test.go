package syncer

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"sort"
	"testing"
)

type Mock struct {
	mock.Mock
}

func (m *Mock) CreateDirectoryMock(location string, dry bool) bool {
	args := m.Called(location, dry)
	return args.Bool(0)
}

func (m *Mock) RunCommandMock(commandSlice []string) {
	m.Called(commandSlice)
}

func (m *Mock) RemoveAllMock(file string) error {
	args := m.Called(file)
	return args.Error(0)
}

/*
func TestCreateDirectoryTrees(t *testing.T) {
	m := new(Mock)
	createDirectory = m.CreateDirectoryMock

	location := "/location/"
	dirFileState := fileState{hash: "directory", state: "PRESENT", timestamp: 0}

	locationState := make(map[string]fileState)
	locationState["dir1"] = dirFileState
	syncedState := make(map[string]fileState)
	syncedState["dir1"] = dirFileState
	syncedState["dir2"] = dirFileState
	syncedState["dir3"] = dirFileState
	syncedState["dir3/dir33"] = dirFileState

	m.On("CreateDirectoryMock", "/location/dir2", true).Return(true)
	m.On("CreateDirectoryMock", "/location/dir3/dir33", true).Return(true)

	createDirectoryTrees(locationStates, syncedState, true)
	m.AssertExpectations(t)
}
*/

func TestOnlyDeepestDirs(t *testing.T) {
	input := make(map[string]state.State)
	dirFileState := state.State{Hash: "directory", State: "PRESENT", Timestamp: 0}

	input["file1"] = state.State{Hash: "abc", State: "PRESENT", Timestamp: 0}
	input["dir1"] = dirFileState
	input["dir1/dir11"] = dirFileState
	input["dir2"] = dirFileState
	input["dir2/dir22"] = dirFileState
	input["dir2/dir22/dir222"] = dirFileState
	input["dir3"] = dirFileState

	expected := []string{"dir3", "dir1/dir11", "dir2/dir22/dir222"}
	sort.Strings(expected)

	result := onlyDeepestDirs(input)
	sort.Strings(result)
	assert.Equal(t, expected, result)
}
