package syncer

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestCheckAll(t *testing.T) {
	dir1, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir1)
	dir2, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir2)

	var tests = []struct {
		uid1           string
		uid2           string
		dry            bool
		expectedString string
		expectedBool   bool
	}{
		{
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			false,
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			true,
		},
		{
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			"",
			false,
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			true,
		},
		{
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			"NOT_UID",
			false,
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			true,
		},
		{
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcNOTGOODUID",
			false,
			"",
			false,
		},
	}

	for _, test := range tests {
		t.Run(test.uid1+","+test.uid2, func(t *testing.T) {
			content := []byte(test.uid1)
			if err := ioutil.WriteFile(filepath.Join(dir1, ".dync-uid"), content, 0666); err != nil {
				log.Fatal(err)
			}
			content = []byte(test.uid2)
			if err := ioutil.WriteFile(filepath.Join(dir2, ".dync-uid"), content, 0666); err != nil {
				log.Fatal(err)
			}
			result, ok := CheckAll([]string{dir1, dir2}, test.dry)
			assert.Equal(t, test.expectedString, result)
			assert.Equal(t, test.expectedBool, ok)
		})
	}

	//Different UIDS
	//EMPTY
	//EMPTY and existing
	// same
	//also check if not verify uid
	//dry or not dry
}

func TestVerifyIdenticalUIDs(t *testing.T) {
	var tests = []struct {
		uids     []string
		expected bool
	}{
		{
			[]string{},
			true,
		},
		{
			[]string{"a", "b", "a"},
			false,
		},
		{
			[]string{"a", "a", "b"},
			false,
		},
		{
			[]string{"a", "a"},
			true,
		},
		{
			[]string{"a", "a", "a"},
			true,
		},
	}

	for _, test := range tests {
		t.Run(strings.Join(test.uids, ","), func(t *testing.T) {
			have := verifyIdenticalUIDs(test.uids)
			assert.Equal(t, test.expected, have)

		})
	}

}

func TestGenerateUID(t *testing.T) {
	uidLength := len(generateUID())
	assert.Equal(t, 512, uidLength)
}

func TestVerifyUID(t *testing.T) {
	var tests = []struct {
		uid      string
		expected bool
	}{
		{
			"",
			false,
		},
		{
			"a",
			false,
		},
		{
			"8859da1a9bb4b148a71574024c5e9131a0aedb3b3c54f392f23f5b3d573e0c35bafb9b25632c23bdcba5ca259698a3e85bc58241b2b1262564793f6d93afc7287938d6a7cbc9a85004ec786fde4d54707756643b31400f7e7cb5a44134824294b57d59b4a6203985ed8a99183f1bb7d331dc972fe727e019d9da1a834636b6f1154bdd0ecd14685f3513648e13ea42842d68bad2455d215255e8c552f5758cf6876d9684021daccb5eb2123a7be7fe679106c2d69a488e773fd31dd3f592ab2b6c670574729663b8408a3a7b3e23f4874e3655407873623d4bbdae40b42f77d1ed81b2f1dd590cccd10b803d9076c8b5a973c0e873aa436b0b50dcd2ad6bed28",
			true,
		},
	}

	for _, test := range tests {
		t.Run(test.uid, func(t *testing.T) {
			have := verifyUID(test.uid)
			assert.Equal(t, test.expected, have)
		})
	}
}

func TestGetUID(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir) // clean up

	content := []byte("FAKE_UID")
	tmpFilePath := filepath.Join(dir, ".dync-uid")
	if err := ioutil.WriteFile(tmpFilePath, content, 0666); err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, "FAKE_UID", getUID(dir))
}

func TestSaveUID(t *testing.T) {
	dir1, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir1) // clean up
	dir2, err := ioutil.TempDir("", "")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir2) // clean up

	saveUID([]string{dir1, dir2}, "TEST", true)
	_, err1 := ioutil.ReadFile(filepath.Join(dir1, ".dync-uid"))
	_, err2 := ioutil.ReadFile(filepath.Join(dir2, ".dync-uid"))
	assert.Equal(t, true, os.IsNotExist(err1))
	assert.Equal(t, true, os.IsNotExist(err2))

	saveUID([]string{dir1, dir2}, "TEST", false)
	uid1, _ := ioutil.ReadFile(filepath.Join(dir1, ".dync-uid"))
	uid2, _ := ioutil.ReadFile(filepath.Join(dir2, ".dync-uid"))
	assert.Equal(t, "TEST", string(uid1))
	assert.Equal(t, "TEST", string(uid2))
}
