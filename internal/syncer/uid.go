package syncer

import (
	"crypto/rand"
	"encoding/hex"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

var uidFile = ".dync-uid"

const uidLength = 512

//CheckAll Retrieve all uids from all locations
func checkMatchingUID(locations []Location, dry bool) (string, bool) {
	log.Println("Checking UID's")
	var uids []string
	var missingUIDLocations []string
	for _, location := range locations {
		uid := getUID(location.FullPath)
		if uid == "" || !verifyUID(uid) {
			missingUIDLocations = append(missingUIDLocations, location.FullPath)
			continue
		}
		uids = append(uids, uid)
	}

	if !verifyIdenticalUIDs(uids) {
		return "", false
	}

	var uid string
	if len(uids) == 0 {
		uid = generateUID()
		saveUID(locations, uid, dry)
	} else {
		uid = uids[0]
	}
	log.Println("UID: " + uid)

	return uid, true
}

func verifyIdenticalUIDs(uids []string) bool {
	uniques := make(map[string]string)
	for _, uid := range uids {
		uniques[uid] = ""
	}
	if len(uniques) <= 1 {
		return true
	}
	return false
}

func generateUID() string {
	key := [uidLength / 2]byte{}
	_, err := rand.Read(key[:])
	if err != nil {
		panic(err)
	}
	return hex.EncodeToString(key[:])
}

func verifyUID(uid string) bool {
	if len(uid) == uidLength {
		return true
	}
	return false
}

func getUID(location string) string {
	//TODO get uid
	//TODO read from file first line of file (if it exists)
	if isLocal(location) {
		uidPath := filepath.Join(location, uidFile)
		info, err := os.Stat(uidPath)
		if os.IsNotExist(err) {
			return ""
		}

		if err != nil {
			log.Println(err)
		}

		if !info.IsDir() {
			uid, err := ioutil.ReadFile(uidPath)
			if err != nil {
				log.Fatal(err)
			}

			return string(uid)
		}
	}
	return ""
}

func saveUID(locations []Location, uid string, dry bool) {
	//TODO use tmp dir or local and copy it to the others
	//Write it to at least one,
	for _, location := range locations {
		if isLocal(location.FullPath) {
			if !dry {
				err := ioutil.WriteFile(filepath.Join(location.FullPath, uidFile), []byte(uid), os.ModePerm)
				if err != nil {
					log.Fatal(err)
				} else {
					continue
				}
			}
		}
	}
	for _, location := range locations {
		if !isLocal(location.FullPath) {
		}
	}
}
