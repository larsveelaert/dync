package syncer

import (
	"bufio"
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path"
	"path/filepath"
	"strings"
)

//CheckProgramInstalled checks if a program (given in string) is installed and reachable on the system
func checkProgramInstalled(program string) bool {
	cmd := exec.Command("/bin/sh", "-c", "command -v "+program)
	if err := cmd.Run(); err != nil {
		return false
	}
	return true
}

//RunCommand runs a command while outputting realtime
func runCommand(command []string) {
	cmd := exec.Command(command[0], command[1:]...)
	stderr, _ := cmd.StderrPipe()
	stdout, _ := cmd.StdoutPipe()
	cmd.Start()
	go func() {
		outputReader(stderr)
	}()
	go func() {
		outputReader(stdout)
	}()
	cmd.Wait()
}

func outputReader(reader io.Reader) {
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		l := scanner.Text()
		fmt.Println(l)
	}
}

//GetAllPaths gets all absolute filepaths in that location (2 return arrays, one for files, one for dirs)
func getAllPaths(location string) []string {
	var allPaths []string
	err := filepath.Walk(location, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatal(err)
		} else {
			allPaths = append(allPaths, path)
		}
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
	return allPaths
}

//GetDepthMap makes a map based on depth
func getDepthMap(paths []string) map[int][]string {
	depthMap := make(map[int][]string)

	for _, path := range paths {
		depth := calculateDepth(path)
		depthMap[depth] = append(depthMap[depth], path)
	}

	return depthMap
}

func calculateDepth(Path string) int {
	depth := -1
	remainingPath := Path
	for remainingPath != "." {
		depth++
		remainingPath = path.Dir(remainingPath)
	}
	return depth
}

//EncryptPath encrypts the path
func encryptPath(uid string, path string) string {
	//TODO
	bytes := sha256.Sum256([]byte(uid + path))
	return hex.EncodeToString(bytes[:])
}

//HashFile gets hash of that file
func hashFile(path string) string {
	maxLength := 16

	if isSymlink(path) {
		dest, err := os.Readlink(path)
		if err != nil {
			log.Fatal(err)
		}
		return md5Hash(dest)
	}

	info, err := os.Stat(path)
	if err != nil {
		log.Fatal(err)
	}

	if info.IsDir() {
		return "directory"
	}

	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	return hex.EncodeToString(h.Sum(nil)[:maxLength])
}

//Md5Hash takes a string and hashes it to a md5 string
func md5Hash(input string) string {
	h := md5.New()
	io.WriteString(h, input)
	return hex.EncodeToString(h.Sum(nil))
}

func isSymlink(path string) bool {
	info, err := os.Lstat(path)
	if err != nil {
		log.Fatal(err)
	}
	if info.Mode()&os.ModeSymlink == os.ModeSymlink {
		return true
	}
	return false
}

//GetFileTimestamp gets the timestamp of the file
func getFileTimestamp(path string) int64 {
	if isSymlink(path) {
		info, err := os.Lstat(path)
		if err != nil {
			log.Fatal(err)
		}
		return info.ModTime().Unix()
	}
	info, err := os.Stat(path)
	if err != nil {
		log.Fatal(err)
	}
	return info.ModTime().Unix()
}

func getHomeDir() string {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return usr.HomeDir
}

//CleanLocations cleans all the locations to replace abreviations and suffix if needed
func cleanLocations(locations []Location) []Location {
	for i, location := range locations {
		locations[i] = cleanLocation(location)
	}
	return locations
}

//CleanLocation cleans all the locations to replace abreviations and suffix if needed
func cleanLocation(location Location) Location {
	location.FullPath = suffixSlash(replaceHome(location.FullPath))
	return location

}

func replaceHome(location string) string {
	if strings.HasPrefix(location, "~") {
		return strings.Replace(location, "~", getHomeDir(), -1)
	}
	return location
}

//isLocal checks if location is local
func isLocal(location string) bool {
	if strings.HasPrefix(location, "/") {
		return true
	}
	return false
}

//LocationExists checks if locaton exists
func locationExists(location string, dry bool) bool {
	if isLocal(location) {
		if assureDirectoryPresent(location, dry) {
			return true
		}
	}
	//TODO implement remote locations
	return false
}

//AssureDirectoryPresent assures that the directory is present and gives feedback
func assureDirectoryPresent(path string, dry bool) bool {
	//TODO dry
	if directoryPresent(path) {
		return true
	}
	return createDirectory(path, dry)
}

//DirectoryPresent checks if directory present
func directoryPresent(path string) bool {
	if isLocal(path) {
		if info, err := os.Stat(path); !os.IsNotExist(err) {
			if info.IsDir() {
				return true
			}
			fmt.Println(path + " is a file, not a directory")
		}
		return false
	}
	return false
}

//CreateDirectory Creates a directory on that location (makes all)
func createDirectory(path string, dry bool) bool {
	if isLocal(path) {
		log.Println("CREATE " + path)
		if !dry {
			err := os.MkdirAll(path, os.ModePerm)
			if err == nil {
				return true
			}
			log.Println("1")
			log.Print(err)
			log.Println("2")
			return false
		}
	}
	//TODO implement remote ways
	return false
}

//LocalizeLocation cleans the location from remote hostnames and suffixes a forwardslash
func localizeLocation(location string) string {
	//TODO remove all remote stuff (localize)
	if strings.HasSuffix(location, "/") {
	} else {
		location = location + "/"
	}
	return location
}

func suffixSlash(location string) string {
	if strings.HasSuffix(location, "/") {
	} else {
		location = location + "/"
	}
	return location
}

//IsPath checks if the string is a path and not empty
func isPath(path string) bool {
	if len(path) > 0 {
		return true
	}
	return false
}

//ListOnlyFiles lists only the files on a path
func listOnlyFiles(path string) []string {
	log.Println("Reading directory...")
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	var onlyFiles []string
	log.Println("Looping over the contents to see if thhey are files")
	for _, f := range files {
		if !f.IsDir() {
			onlyFiles = append(onlyFiles, f.Name())
		}
	}
	return onlyFiles
}
