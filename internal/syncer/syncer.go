package syncer

//TODO progressbar? "github.com/BTBurke/clt" -> or rather a counter amount of files that ar

import (
	"errors"
	"fmt"
)

var workdir = ".dync-workdir"

//Syncer struct holders the sync function
type Syncer struct {
	DryRun bool
}

type Location struct {
	FullPath string
}

//Sync syncronizes the given locations
func (s *Syncer) Sync(locations []Location) error {
	//clean
	locations = cleanLocations(locations)
	//Get the uids
	uid, ok := checkMatchingUID(locations, s.DryRun)
	if !ok {
		return errors.New("UIDs of folders don't match, delete history if you want to force sync")
	}
	//Get information
	fmt.Println("Retrieving history information")
	histories := make(map[string]interface{})
	for _, location := range locations {
		histories[location.FullPath] = retrieveHistory(location)
	}

	locationStates := getLocationStates(uid, locations, histories, s.DryRun)
	//Process
	syncedState := syncStates(uid, locationStates, histories)
	//Perform
	performSyncActions(locationStates, syncedState, s.DryRun)
	//Save the history
	writeHistory(uid, histories, syncedState, s.DryRun)

	return nil
}
