package syncer

import (
	"log"
	"path/filepath"
	"sort"
	"strings"
)

//Perform does all the actions of the sync process
func performSyncActions(locationStates map[string]interface{}, syncedState map[string]State, dry bool) {
	log.Println("Performing actions")
	fileOperationOrder := getFileOperationOrder(syncedState)
	for location, locationState := range locationStates {
		createDirectoryTree(location, locationState.(map[string]State), syncedState, dry)
	}
	for location := range locationStates {
		findAndCopy(location, locationStates, syncedState, fileOperationOrder, dry)
		//locationStates[location] = generateState(location) //TODO is this needed? -> mainly for all the changes that happened between runs ... maybe have a max time spent before regenerating???
	}
	for location := range locationStates {
		deleteFiles(location, locationStates, syncedState, dry)
	}
	for location := range locationStates {
		deleteFolders(location, locationStates, syncedState, dry)
	}
}

func getFileOperationOrder(syncedState map[string]State) []string {
	var fileOperationOrder []string
	for path := range syncedState {
		fileOperationOrder = append(fileOperationOrder, path)
	}
	sort.Slice(fileOperationOrder, func(i, j int) bool {
		return syncedState[fileOperationOrder[i]].Timestamp < syncedState[fileOperationOrder[j]].Timestamp
	})
	return fileOperationOrder
}

func createDirectoryTree(location string, locationState map[string]State, syncedState map[string]State, dry bool) {
	for _, path := range onlyDeepestDirs(syncedState) {
		if _, ok := locationState[path]; !ok && syncedState[path].State == "PRESENT" {
			createDirectory(filepath.Join(location, path), dry)
		}
	}
}

//Sync transfers files from wherever they are available
func findAndCopy(location string, locationStates map[string]interface{}, syncedState map[string]State, fileOperationOrder []string, dry bool) {
	//TODO can be optmized by using the locationstate -> prior knowledge? -> but what if edits happened in the meanwhile -> if still same file, as descibed above -> should be function anyway
	//TODO -> regenerate locaiton state before copying, and use this information or recheck before the copy or delete or to really copy or delete or ...
	for _, path := range fileOperationOrder {
		filestate := syncedState[path]
		if !isDir(filestate) && filestate.State == "PRESENT" {
			present := false
			if _, ok := locationStates[location].(map[string]State)[path]; ok {
				present = true
			}
			if !present || filestate.Hash != locationStates[location].(map[string]State)[path].Hash || (filestate.Hash != locationStates[location].(map[string]State)[path].Hash && filestate.Timestamp != locationStates[location].(map[string]State)[path].Timestamp) {
				//TODO back up file if exists
				//TODO check if exists -> remember remotes
				ok := backupFile(location, path, dry)
				if ok {
					searchAndTryCopyFile(locationStates, path, filestate, location, dry)
				}
			}
		}
	}
}

func deleteFiles(location string, locationStates map[string]interface{}, syncedState map[string]State, dry bool) {
	//TODO can be optmized by using the locationstate -> prior knowledge? -> but what if edits happened in the meanwhile -> if still same file, as descibed above -> should be function anyway
	//TODO -> regenerate locaiton state before copying, and use this information or recheck before the copy or delete or to really copy or delete or ...
	for path, filestate := range syncedState {
		if !isDir(filestate) && filestate.State == "DELETED" {
			ok := backupFile(location, path, dry)
			if ok {
				deleteFile(filepath.Join(location, path), dry)
			}
		}
	}
}

func deleteFolders(location string, locationStates map[string]interface{}, syncedState map[string]State, dry bool) {
	//TODO can be optmized by using the locationstate -> prior knowledge? -> but what if edits happened in the meanwhile -> if still same file, as descibed above -> should be function anyway
	//TODO -> regenerate locaiton state before copying, and use this information or recheck before the copy or delete or to really copy or delete or ...
	for path, filestate := range syncedState {
		if isDir(filestate) && filestate.State == "DELETED" {
			deleteFile(filepath.Join(location, path), dry)
			//TODO delete all folders (highest)
		}
	}
}

func onlyDeepestDirs(syncedState map[string]State) []string {
	var paths []string
	for path, filestate := range syncedState {
		if isDir(filestate) {
			paths = append(paths, path)
		}
	}
	depthMap := getDepthMap(paths)
	deepestPaths := depthMap[len(depthMap)-1]
	for i := len(depthMap) - 2; i >= 0; i-- {
		for _, path := range depthMap[i] {
			deepest := true
			for _, deepestDir := range deepestPaths {
				if strings.HasPrefix(deepestDir, path) {
					deepest = false
					break
				}
			}
			if deepest {
				deepestPaths = append(deepestPaths, path)
			}
		}
	}
	return deepestPaths
}

func isDir(filestate State) bool {
	if filestate.Hash == "directory" {
		return true
	}
	return false
}

func searchAndTryCopyFile(locationStates map[string]interface{}, path string, filestate State, location string, dry bool) {
	for origin, locationState := range locationStates {
		if isLocal(origin) {
			for path, filestateAtLocation := range locationState.(map[string]State) {
				if filestateAtLocation.Hash == filestate.Hash {
					if copyFile(origin+path, filepath.Join(location, path), dry) { //TODO get the parentdirectory of the path and append it to location
						return
					}
				}

			}
		}
	}

	for location := range locationStates {
		if !isLocal(location) {
			//TODO then search under the specific paths on all the remotes
			//TODO match hash
			//TODO try copy, if succesful abort
		}
	}

	//TODO then search all files on all remotes
	//TODO match hash
	//TODO try copy, if succesful abort

	//TODO from the moment it is found somewhere, copy it with rsync and return true if succeeded, otherwise continue to the next location

	//TODO for ssh support remote moves ->

}
