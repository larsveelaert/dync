package syncer

import (
	"github.com/stretchr/testify/mock"
	"testing"
)

type Mock struct {
	mock.Mock
}

func (m *Mock) DeleteLocalFileMock(path string, dry bool) bool {
	args := m.Called(path, dry)
	return args.Bool(0)
}

func TestDeleteFile(t *testing.T) {
	m := new(Mock)

	deleteLocalFileOrg := deleteLocalFile
	deleteLocalFile = m.DeleteLocalFileMock
	defer func() { deleteLocalFile = deleteLocalFileOrg }()

	m.On("DeleteLocalFileMock", "/local/test1", true).Return(true)
	m.On("DeleteLocalFileMock", "/local/test1", false).Return(true)
	DeleteFile("/local/test1", true)
	DeleteFile("ssh://test1", true)
	DeleteFile("/local/test1", false)
	DeleteFile("ssh://test1", false)
	m.AssertExpectations(t)
	//TODO this is ugly ... cant it be changed for once ...
	//variables on a struct, so I can assign this stuff .. ok .. but really cumbersome, now, I can just make a function a variable and done
	//I can inspect the lenght of the args.. but yeah
}

/*
func TestDeleteLocalFile(t *testing.T) {
	m := new(Mock)

	removeAllOrg := m.RemoveAllMock
	removeAll = m.RemoveAllMock
	defer func() { removeAll = removeAllOrg }()

	m.On("removeAllMock", "/local/test1").Return(nil)
	deleteLocalFile("/local/test1", true)
	//-> this is calling the real mock thing I guess TODO
	deleteLocalFile("/local/test1", false)
	m.AssertExpectations(t)
}
*/
