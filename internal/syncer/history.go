package syncer

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func retrieveHistory(location Location) map[string]State {
	fmt.Println("Retrieving history from " + location.FullPath)
	history := make(map[string]State)
	if isLocal(location.FullPath) {
		//TODO detect the existance of the 1 history file and check if it is the latest
		//TODO here is the debate of 1 file versus multiple files going on
		// - corruption of the 1 file ... could be an issue ...
		// - but writes to xK files is too heavy

		//One file

		dat, err := ioutil.ReadFile(filepath.Join(location.FullPath, workdir, "history"))
		if err != nil {
			log.Println(err)
			return history
		}

		//TODO check corruption

		for _, line := range strings.Split(string(dat), "\n") {
			splittedLine := strings.Split(line, " ")
			if len(splittedLine) != 4 {
				continue
			}
			if !verifyHistoryData(splittedLine[1:3]) {
				continue
			}
			pathHash := splittedLine[0]
			history[pathHash] = generateFileState(splittedLine[1:3])

		}

		//Multiple files

		/*
			historyPath := filepath.Join(location, workdir, "objects")
			log.Println("using " + historyPath)
			if !utils.DirectoryPresent(historyPath) {
				log.Println("location is not present, so returning empty history")
				return history
			}

			log.Println("Getting all history objects...")
			historyFiles := utils.ListOnlyFiles(historyPath)

			for i, pathHash := range historyFiles {
				log.Println("[Retrieving history from " + location + "] processing file (" + strconv.Itoa(i) + ") with pathhash: " + pathHash)
				dat, err := ioutil.ReadFile(filepath.Join(historyPath, pathHash))
				if err != nil {
					log.Fatal(err)
				}
				firstLine := strings.Split(string(dat), "\n")[0]
				splittedLine := strings.Split(firstLine, " ")
				if !verifyHistoryData(splittedLine) {
					continue
				}
				history[pathHash] = generateFileState(splittedLine)
			}
		*/

	} else {
		//TODO make sure there is a temp_history folder for history
		//TODO if it exists remove it
		//TODO then recreate it
		//TODO download the remote history with rsync??
	}
	return history
}

//Write history to locations
func writeHistory(uid string, histories map[string]interface{}, syncedState map[string]State, dry bool) {
	//TODO use tmpdir or a local and copy everything to all locations (now we do not suppor remotes)
	log.Println("Hashing all paths before saving synced state to history...")
	fileStateMaps := []map[string]State{convertPathsToHashes(uid, syncedState)}

	log.Println("Compiling current sate and previous history before saving...")
	for _, history := range histories {
		fileStateMaps = append(fileStateMaps, history.(map[string]State))
	}
	mergedState := mergeFileStateMaps(fileStateMaps)

	log.Println("Writing history to all locations")
	for location := range histories {
		if isLocal(location) {
			historyPath := filepath.Join(location, workdir, "objects")
			if assureDirectoryPresent(historyPath, dry) {
				/*
					Aee discussion above
					log.Println("Writing history into seperate files...")
					for pathHash, state := range mergedState {
						writeFileState(filepath.Join(historyPath, pathHash), state, dry)
					}
				*/
				writeAllFileStates(filepath.Join(location, workdir, "history"), mergedState, dry)
			}
		}
	}
}

func convertPathsToHashes(uid string, fileStateMap map[string]State) map[string]State {
	convertedFileStateMap := make(map[string]State)
	for path, filestate := range fileStateMap {
		convertedFileStateMap[encryptPath(uid, path)] = filestate
	}
	return convertedFileStateMap
}

func mergeFileStateMaps(fileStateMaps []map[string]State) map[string]State {
	//TODO remove all history that is older that 1Y?
	mergedFileStateMap := make(map[string]State)
	for _, fileStateMap := range fileStateMaps {
		for pathHash, state := range fileStateMap {
			if mergedState, ok := mergedFileStateMap[pathHash]; ok {
				if state.Timestamp > mergedState.Timestamp {
					mergedFileStateMap[pathHash] = state
				}
				continue
			}
			mergedFileStateMap[pathHash] = state
		}
	}
	return mergedFileStateMap
}

func generateFileState(splittedLine []string) State {
	timestamp, err := strconv.ParseInt(splittedLine[0], 10, 64)
	if err != nil {
		panic(err)
	}
	hash := splittedLine[1]
	filestate := splittedLine[2]
	return State{Timestamp: timestamp, Hash: hash, State: filestate}
}

func verifyHistoryData(data []string) bool {
	if len(data) != 3 {
		return false
	}
	timestamp := data[0]
	if _, err := strconv.Atoi(timestamp); err != nil {
		return false
	}
	return true
}

func writeFileState(path string, filestate State, dry bool) {
	hash := filestate.Hash
	if hash == "" {
		hash = "empty"
	}
	if !dry {
		err := ioutil.WriteFile(path, []byte(strconv.FormatInt(filestate.Timestamp, 10)+" "+hash+" "+filestate.State), os.ModePerm)
		if err != nil {
			log.Fatal(err)
		}
	}

}

func writeAllFileStates(path string, mergedState map[string]State, dry bool) {
	var contents []byte
	for pathHash, filestate := range mergedState {
		hash := filestate.Hash
		if hash == "" {
			hash = "empty"
		}
		contents = append(contents, []byte(pathHash+" "+strconv.FormatInt(filestate.Timestamp, 10)+" "+hash+" "+filestate.State+"\n")...)
	}
	if !dry {
		err := ioutil.WriteFile(path, contents, os.ModePerm)

		if err != nil {
			log.Fatal(err)
		}
	}

}
