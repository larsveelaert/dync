package syncer

import (
	"fmt"
	"log"
	"os"
)

//CopyFile Copies a file from origin to destination
var copyFile = func(origin string, destination string, dry bool) bool {
	//TODO copyfile should only copy, regardless of the existence of the directory?
	//TODO way of syncing should be done as best as possible by availability
	//TODO rsync > cp or windows alternative if local > scp > native go read and write maybe remote

	//TODO or should I use assureDir every time?? -> could be wastefull -> should be done before??? -> but otherwise here
	//TODO assureDir should use the best available option for that location (local? (mkdirAll) remote? ftp or scp? ssh mkdir or windows variant

	log.Println("COPY " + origin + " -> " + destination)

	if checkProgramInstalled("rsync") {
		if !dry {
			runCommand([]string{"rsync", "-t", "--links", origin, destination})
		}
	} else {
		fmt.Println("ERROR: rsync is not installed")
		os.Exit(1)
	}
	return true
}

//BackupFile backups a file
func backupFile(location string, path string, dry bool) bool {
	if !dry {
		return true
	}
	//TODO
	return true
}

//DeleteFile deletes a file
var deleteFile = func(path string, dry bool) bool {
	log.Println("DELETE " + path)
	if isLocal(path) {
		//return s.deleteLocalFile(path, dry)
		return deleteLocalFile(path, dry)
	}
	//TODO remote
	return false
}

var removeAll = os.RemoveAll

var deleteLocalFile = func(path string, dry bool) bool {
	if !dry {
		err := removeAll(path)
		if err != nil {
			log.Print(err)
			return false
		}
	}
	return true
}
