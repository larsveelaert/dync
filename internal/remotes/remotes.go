package remotes

import (
	"io/ioutil"
	"log"
	"path"
	"strings"
)

//Get returns the remotes found in the file
func Get(folder string, remotesfile string) []string {
	//TODO join these params and read the file, split the lines
	//TODO verify each line if it is a proper remote
	remotesfilepath := path.Join(folder, remotesfile)
	contents, err := ioutil.ReadFile(remotesfilepath)
	if err != nil {
		log.Print(err)
	}

	var remotes []string
	for _, remote := range strings.Split(string(contents), "\n") {
		if remote != "" {
			remotes = append(remotes, remote)
		}
	}

	log.Println("Found the following remotes in " + remotesfilepath + " :" + strings.Join(remotes, ","))

	//TODO if 0 remotes or the file is not found, give a proper message
	return remotes
}
